﻿using System.IO;
using UnityEngine;
using Utilities;
using XmlClasses;

namespace Inventory
{
    public class Effects : MonoBehaviour
    {
        private Sign.SignData data = null;
        private int printIndex = 0;

        public void Heal(int amount)
        {
            Debug.Log("Player was healed.");
        }

        public void Revive()
        {
            Debug.Log("Player was revived.");
        }

        public void DisplayMessage(string key)
        {
            if (data == null)
            {
                data = GetMessage(key);
            }
            else if (printIndex >= data.Message.Count)
            {
                Manager.Instance.Printer.TextBox.SetActive(false);
                Manager.Instance.Printer.ClearTextbox();
                printIndex = 0;
                data = null;
            }

            if (data != null)
            {
                Manager.Instance.Printer.Print(data.Message[printIndex], true);
                printIndex++;
            }
        }

        public Sign.SignData GetMessage(string key)
        {
            var path = Path.Combine(Application.dataPath, "Resources", Manager.Instance.SignXmlFile.name + ".xml");
            var container = XmlReader.GetXmlData<Sign.SignDataContainer>(path).signCollection;

            foreach (var sign in container)
            {
                if (sign.name == key)
                {
                    return sign;
                }
            }

            return null;
        }
    }
}
    