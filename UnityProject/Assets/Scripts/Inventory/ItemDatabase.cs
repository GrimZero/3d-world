﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Extensions;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Inventory
{
    [Serializable]
    [CreateAssetMenu(fileName = "Item list", menuName = "Resources/Item list")]
    public class ItemDatabase : ScriptableObject
    {
        public List<GameObject> Items = new List<GameObject>();

        public void AddToList(GameObject item)
        {
            Manager.Instance.ItemContainerObject.Items.AddIfNotContains(item);

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        public void AddToList(IEnumerable<GameObject> items)
        {
            foreach (var gameObject in items)
            {
                AddToList(gameObject);
            }
        }

        public void RemoveFromList(GameObject item)
        {
            Items.RemoveIfContains(item);
        }
    }
}