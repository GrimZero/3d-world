﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Utilities.Extensions;

namespace Inventory
{
    [Serializable]
    public class Item : MonoBehaviour
    {
        [HideInInspector, SerializeField] private Sprite _itemSprite;
        [HideInInspector, SerializeField] private string _itemName = string.Empty;
        [HideInInspector, SerializeField] private string _itemDescription = string.Empty;
        [HideInInspector, SerializeField] private int _maxStackSize = 1;
        [HideInInspector] public UnityEvent _useItem = null;
        public ItemAmountDictionary components = new ItemAmountDictionary();

        [HideInInspector, SerializeField]
        public bool IsDecomposable => components.Count != 0;

        public Sprite ItemSprite
        {
            get => _itemSprite;
            set => _itemSprite = value;
        }
        public string ItemName
        {
            get => _itemName;
            set => _itemName = value;
        }
        public string ItemDescription
        {
            get => _itemDescription;
            set => _itemDescription = value;
        }
        public int StackSize
        {
            get => _maxStackSize;
            set => _maxStackSize = value;
        }

        public UnityEvent Effect() { return _useItem; }
    }
}

