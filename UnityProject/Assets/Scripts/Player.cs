﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Player : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 1f;
    public float GetMovementSpeed() { return _movementSpeed; }

    [SerializeField] private float _maxHealth = 100;
    public float _currentHealth { get; set; }
}