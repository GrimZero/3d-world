﻿using UnityEngine;

namespace Utilities
{
    public class CameraController : MonoBehaviour
    { 
        [SerializeField] private GameObject _player;
        private float multiplier = 5;

        void Reset()
        {
            _player = Manager.Instance.PlayerGameobject;
        }

        void Update ()
        {
            if (_player != null)
            {
                transform.LookAt(_player.transform.position);

                transform.Rotate(Input.GetAxis("Mouse X") * multiplier, Input.GetAxis("Mouse Y") * multiplier, 0);
            }
        }
    }
}
