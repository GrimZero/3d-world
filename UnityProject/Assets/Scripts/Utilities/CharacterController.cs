﻿using UnityEngine;
using UnityEngine.AI;

namespace Utilities
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField, Header("Axis setup")] private string _horizontalAxis = "Horizontal";
        [SerializeField] private string _verticalAxis = "Vertical";
        [SerializeField] private string _jumpAxis = "Jump";

        private Vector2 moveForce;

        [SerializeField, Header("Move forces")] private float jumpForce = 10f;
        [SerializeField] private float moveSpeed = 10f;

        [SerializeField] private GameObject _playerMeshObject;

        private NavMeshAgent _agent;
        private Rigidbody _rig;

        void Start()
        {
            _agent = GetComponent<NavMeshAgent>();
            _rig = GetComponent<Rigidbody>();
        }

        public void SetDestination(Vector3 destination)
        {
            _agent.destination = destination;
        }

        private void StoreInput()
        {
            moveForce = new Vector2(Input.GetAxisRaw(_horizontalAxis), Input.GetAxisRaw(_verticalAxis));
            moveForce *= moveSpeed;
        }

        private void ExecuteMovement()
        {
            _rig.AddForce(moveForce.x, 0, moveForce.y);
            if (Input.GetButtonDown(_jumpAxis))
            {
                _rig.AddForce(0, jumpForce, 0, ForceMode.Impulse);
            }
        }

        public void Update()
        {
            StoreInput();
            ExecuteMovement();

            _playerMeshObject.GetComponent<Animator>().SetFloat("deltaSpeed", _rig.velocity.magnitude);
        }
    }
}