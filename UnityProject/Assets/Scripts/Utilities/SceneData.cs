﻿using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public enum SceneType
    {
        Menu,
        Level,
        Options,
        Default
    }

    [CreateAssetMenu(menuName = "Resources/SceneData")]
    public class SceneData : ScriptableObject
    {
        public Dictionary<string, SceneType> scenes = new Dictionary<string, SceneType>();
    }
}