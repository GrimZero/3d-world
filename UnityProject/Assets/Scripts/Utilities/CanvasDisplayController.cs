﻿using System.Collections.Generic;
using UnityEngine;
using Utilities.Extensions;

namespace Utilities
{
    public class CanvasDisplayController : MonoBehaviour
    {
        [SerializeField] private StringObjectDictionary _canvasSwitchableElements;
        [SerializeField, Space] private readonly List<GameObject> _objectsEnabledOnStartup = new List<GameObject>();

        private void Disable(GameObject gO) { gO.SetActive(false); }

        /// <summary>
        /// Enables an element from the dictionary, disables the others by default
        /// </summary>
        /// <param name="toEnable">The GameObject that needs to be enabled</param>
        /// <param name="toDisable">which elements to disable, default: remove none</param>
        public void EnableElement(string toEnable, List<string> toDisable = null)
        {
            //if there are elements to be hidden
            if (toDisable != null)
            {
                foreach (var key in _canvasSwitchableElements.Keys)
                {
                    if (toDisable.Contains(key)) Disable(_canvasSwitchableElements[key]);
                }
            }

            if (_canvasSwitchableElements.ContainsKey(toEnable)) _canvasSwitchableElements[toEnable].SetActive(true);
        }

        public GameObject GetGameObjectByKey(string key)
        {
            if (_canvasSwitchableElements.TryGetValue(key, out GameObject value))
            {
                return value;
            }
            else return null;
        }

        public void Start()
        {
            foreach (var item in _canvasSwitchableElements)
            {
                Disable(item.Value);
            }

            foreach (var item in _objectsEnabledOnStartup)
            {
                item.SetActive(true);
            }
        }
    }
}