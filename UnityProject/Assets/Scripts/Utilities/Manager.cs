﻿using System;
using Inventory;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace Utilities
{
    [Serializable] public class Manager : Singleton<Manager>
    {
        [SerializeField] private int _currTab = 0;
        public int CurrTab { get { return _currTab; } set { _currTab = value; } }

        #region SceneManagement
        [HideInInspector] [SerializeField] private SceneData _levelData;
        public SceneData LevelData { get { return _levelData; } set { _levelData = value; } }

        public Scene GetSceneByName(string name) { return SceneManager.GetSceneByName(name); }
        public void ReloadScene() { GetSceneByName(SceneManager.GetActiveScene().name); }
        #endregion

        [Header("Player")]
        #region PlayerManagement
        [HideInInspector] [SerializeField] private GameObject _playerGameobject;
        public GameObject PlayerGameobject { get { return _playerGameobject; } set { _playerGameobject = value; } }
        public NavMeshAgent PlayerNavigator => _playerGameobject.GetComponent<NavMeshAgent>();
        #endregion

        [Header("ItemHandling")]
        #region Items
        [HideInInspector] [SerializeField] private GameObject _inventory;
        public GameObject Inventory { get { return _inventory; } set { _inventory = value; } }

        [HideInInspector] [SerializeField] private ItemDatabase _items;
        public ItemDatabase ItemContainerObject { get { return _items; } set { _items = value; } }
        #endregion

        [Header("TextManager")]
        #region UI
        [SerializeField] private GameObject _canvas;
        public GameObject Canvas { get { return _canvas; } set { _canvas = value; } }

        [SerializeField] private Sprite _playerIcon;
        public Sprite PlayerIcon { get { return _playerIcon; } set { _playerIcon = value; } }

        [SerializeField] private Printer _printer;
        public Printer Printer { get
        {
            if (!_printer) _printer = GetComponent<Printer>();
            return _printer;
        }}
        #endregion

        [Header("Time")]
        #region Time
        [SerializeField] private TimeCycle _timeCycle;
        public TimeCycle.TimeAndFrame TimeCycle
        {
            get
            {
                if (!_timeCycle) _timeCycle = GetComponent<TimeCycle>();
                return _timeCycle.GetCurrentLocalTime();
            }
        }

        #endregion

        [Header("Files")]
        #region Files
        [SerializeField] private TextAsset _signXmlFile;
        public TextAsset SignXmlFile { get => _signXmlFile; set => _signXmlFile = value; }
        [SerializeField] private TextAsset _storyXmlFile;
        public TextAsset StoryXmlFile { get => _storyXmlFile; set => _storyXmlFile = value; }
        #endregion

        #region GameData
        public string GameName => Application.productName;
        public string CompanyName => Application.companyName;
        #endregion

        void Reset()
        {
            PlayerGameobject = GameObject.FindGameObjectWithTag("Player");
            if (GameObject.FindGameObjectWithTag("Main Canvas"))
            {
                Canvas = GameObject.FindGameObjectWithTag("Main Canvas");
            }

            SignXmlFile = Resources.Load<TextAsset>("TextFiles/Signs");
            StoryXmlFile = Resources.Load<TextAsset>("TextFiles/Story");
        }
    }
}