﻿using System;
using UnityEngine;

namespace Utilities.Extensions
{
    public class ReadOnlyAttribute : PropertyAttribute { }

    [Serializable] public class StringObjectDictionary : SerializableDictionary<string, GameObject> { }
    [Serializable] public class StringSpriteDictionary : SerializableDictionary<string, Sprite> { }
    [Serializable] public class GameObjectFloatDictionary: SerializableDictionary<GameObject, float> { }
    [Serializable] public class ItemAmountDictionary: SerializableDictionary<GameObject, int> { }
}