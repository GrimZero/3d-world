﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Utilities.Extensions.Monobehaviour
{
    [RequireComponent(typeof(Image)), AddComponentMenu("UI/HoldButton", 30)]
    public class HeldButton : Button
    {
        [SerializeField] private UnityEvent _onHold;
        public UnityEvent OnHold { get => _onHold; }

        public override void OnPointerDown(PointerEventData pointerEventData)
        {
            routine = StartCoroutine(ButtonDown());
        }

        public override void OnPointerUp(PointerEventData pointerEventData)
        {
            StopCoroutine(routine);
        }

        private Coroutine routine;

        public IEnumerator ButtonDown()
        {
            while (true)
            {
                OnHold.Invoke();
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
