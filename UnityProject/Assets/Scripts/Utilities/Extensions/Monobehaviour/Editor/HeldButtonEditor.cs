﻿using UnityEditor;

namespace Utilities.Extensions.Monobehaviour.Editor
{
    [CustomEditor(typeof(HeldButton))]
    public class HeldButtonEditor : UnityEditor.UI.ButtonEditor
    {
        public SerializedProperty OnHold;

        private new void OnEnable()
        {
            OnHold = serializedObject.FindProperty("_onHold");
        }

        public override void OnInspectorGUI()
        { 
            base.OnInspectorGUI();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(OnHold);
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
