﻿using System.Collections.Generic;

namespace Utilities.Extensions
{
    public static class ArrayExtensionMethods
    {
        public static bool RemoveIfContains<T>(this List<T> list, T obj)
        {
            if (list.Contains(obj))
            {
                list.Remove(obj);
                return true;
            }
            return false;
        }

        public static bool AddIfNotContains<T>(this List<T> list, T obj)
        {
            if (!list.Contains(obj))
            {
                list.Add(obj);
                return true;
            }

            return false;
        }

        public static void AddRangeIfNotContains<T>(this List<T> list, IEnumerable<T> other)
        {
            foreach (var item in other)
            {
                list.AddIfNotContains(item);
            }
        }

        public static void RemoveRangeIfContains<T>(this List<T> list, IEnumerable<T> other)
        {
            foreach (var item in other)
            {
                list.RemoveIfContains(item);
            }
        }

        public static T Random<T>(this IList<T> list)
        {
            return list.Count == 0 ? default(T) : list[list.RandomIndex()];
        }

        public static int RandomIndex<T>(this IList<T> list)
        {
            return UnityEngine.Random.Range(0, list.Count);
        }
    }
}
