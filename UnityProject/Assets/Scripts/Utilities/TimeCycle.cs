﻿using System;
using UnityEngine;
using Utilities.Extensions;

namespace Utilities
{
    [ExecuteInEditMode]
    public class TimeCycle : MonoBehaviour
    {
        public struct TimeAndFrame
        {
            public DateTime ExactTime;
            public TimeFrame TimeName;

            public TimeAndFrame(DateTime currTime, TimeFrame frame) { ExactTime = currTime; TimeName = frame; }
        }

        [SerializeField] public DateTime DateTime;
        [field: ReadOnly] public TimeFrame LocalTime { get; private set; } = TimeFrame.Invalid;

        public enum TimeFrame
        {
            Night,
            Morning,
            Noon,
            Afternoon,
            Evening,
            Midnight,
            Invalid
        }

        void Update()
        {
            TimeAndFrame timeAndFrame = GetCurrentLocalTime();

            LocalTime = timeAndFrame.TimeName;
            DateTime = timeAndFrame.ExactTime;
        }

        TimeAndFrame GetMomentOfDay(DateTime time)
        {
            float timeConverted = (time.Hour + (time.Minute / 60f));
            int timeFinal = (int)(timeConverted * 100);
            TimeFrame frame = TimeFrame.Invalid;

            if (timeFinal >= 0010) frame = TimeFrame.Night;
            else if (timeFinal >= 0600) frame = TimeFrame.Morning;
            else if (timeFinal >= 1130) frame = TimeFrame.Noon;
            else if (timeFinal >= 1230) frame = TimeFrame.Afternoon;
            else if (timeFinal >= 1800) frame = TimeFrame.Evening;
            else if (timeFinal >= 2200) frame = TimeFrame.Night;
            else if (timeFinal >= 2350) frame = TimeFrame.Midnight;

            return new TimeAndFrame(time, frame);
        }

        public TimeAndFrame GetCurrentLocalTime()
        {
            return GetMomentOfDay(DateTime.Now);
        }

        public TimeSpan GetTimeDifference(DateTime start, DateTime end)
        {
            return end - start;
        }
    }
}
