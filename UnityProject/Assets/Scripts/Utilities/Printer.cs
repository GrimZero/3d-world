﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class Printer : MonoBehaviour
    {
        public GameObject TextBox;

        private GameObject outputBox = null;
        private Text _textComponent;

        [SerializeField, Tooltip("delay in milliseconds")]
        private int _letterDelay = 50;

        private void Reset()
        {
            outputBox = Manager.Instance.Canvas.GetComponent<CanvasDisplayController>().GetGameObjectByKey("TextBox");
            TextBox = outputBox.transform.parent.gameObject;

            if (Manager.Instance.Canvas)
            {
                _textComponent = outputBox.GetComponent<Text>();
            }
        }

        public void Print(string toPrint, bool clear = false)
        {

            TextBox.SetActive(true);
            outputBox.SetActive(true);

            if (clear) ClearTextbox();
            DelayPrint(toPrint);
        }

        public void ClearTextbox()
        {
            _textComponent.text = string.Empty;
        }

        async void DelayPrint(string toPrint)
        {
            foreach (char letter in toPrint)
            {
                _textComponent.text += letter;
                await Task.Delay(_letterDelay);
            }
        }
    }
}
