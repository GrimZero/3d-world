﻿using UnityEngine;

namespace Utilities
{
    public class MouseRay : MonoBehaviour
    {
        [SerializeField] private static string _groundTag = "Ground";

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if(Physics.Raycast(ray.origin, ray.direction, out RaycastHit rayCastHit, Mathf.Infinity))
                {
                    //if it is an object with an OnClick function
                    if (rayCastHit.transform.GetComponent<ClickableObject>())
                    {
                        rayCastHit.transform.GetComponent<ClickableObject>().Activate();
                    }
                }
            }
        }
    }
}
