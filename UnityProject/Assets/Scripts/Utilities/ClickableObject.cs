﻿using UnityEngine;
using UnityEngine.Events;

namespace Utilities
{
    public class ClickableObject : MonoBehaviour
    {
        [SerializeField] private UnityEvent _onClick;

        [HideInInspector] public bool IsActivatable { get; set; } = false;

        public void Activate()
        {
            if (IsActivatable)
            {
                _onClick.Invoke();
            }
        }
    }
}
