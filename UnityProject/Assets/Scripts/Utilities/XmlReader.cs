﻿using System.IO;
using System.Xml.Serialization;

namespace Utilities
{
    public static class XmlReader
    {
        public static T GetXmlData<T>(string path) where T : class
        {
            var serializer = new XmlSerializer(typeof(T));
            var stream = new FileStream(path, FileMode.Open);
            var container = serializer.Deserialize(stream) as T;
            stream.Close();

            return container;
        }
    }
}
