﻿using UnityEngine;

namespace Utilities
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Start ()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
