﻿using System;
using UnityEngine;
using ZXing;
using ZXing.QrCode;

namespace Utilities
{
    public class Qr
    {
        private Color32[] Encode(string textForEncoding, int width, int height)
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Height = height,
                    Width = width
                }
            };
            return writer.Write(textForEncoding);
        }
        public Texture2D GenerateQr(string text)
        {
            var encoded = new Texture2D(256, 256);
            var color32 = Encode(text, encoded.width, encoded.height);
            encoded.SetPixels32(color32);
            encoded.Apply();
            return encoded;
        }

        private WebCamTexture CreateCamera()
        {
            return new WebCamTexture
            {
                requestedHeight = Screen.height,
                requestedWidth = Screen.width
            };
        }

        public string ReadQr()
        {
            var cam = CreateCamera();
            if (cam) cam.Play();

            try
            {
                if (cam.isPlaying)
                {
                    var result = new BarcodeReader().Decode(cam.GetPixels32(), cam.width, cam.height);
                    if (result != null)
                    {
                        var qrTranslated = result.Text;
                        return qrTranslated == string.Empty ? qrTranslated : "Invalid";
                    }
                }
            }
            catch (Exception ex) { Debug.LogWarning(ex.Message); }
            return string.Empty;
        }
    }
}
