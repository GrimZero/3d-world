﻿using UnityEngine;

[ExecuteInEditMode]
public class PortalPullProperties : MonoBehaviour
{
    [SerializeField] private GameObject _clippingPlane = null;
    [SerializeField] private bool _renderClippingPlane = false;

    private Material material = null;
    private MeshRenderer renderer = null;

    void Update()
    {
        renderer = gameObject.GetComponent<MeshRenderer>();
        material = renderer.sharedMaterial;

        Vector3 position = new Vector3();
        Vector3 rotation = new Vector3();

        if (_clippingPlane)
        {
            _clippingPlane.SetActive(_renderClippingPlane);

            position = _clippingPlane.transform.position;
            rotation = _clippingPlane.transform.forward;
        }

        material.SetVector("_ClippingPlanePosition",
            new Vector4(position.x, position.y, position.z));

        material.SetVector("_ClippingPlaneOrientation",
            new Vector4(rotation.x, rotation.y, rotation.z));
    }
}