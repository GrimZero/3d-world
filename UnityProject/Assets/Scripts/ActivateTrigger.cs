﻿using UnityEngine;
using Utilities;

public class ActivateTrigger : MonoBehaviour
{
    private ClickableObject _clickable;

    private void Start()
    {
        _clickable = gameObject.transform.parent.GetComponent<ClickableObject>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_clickable && other.gameObject.CompareTag("Player"))
        {
            _clickable.IsActivatable = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_clickable && other.gameObject.CompareTag("Player"))
        {
            _clickable.IsActivatable = false;
        }
    }
}
