﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlClasses
{
    public class Sign
    {
        public class SignData
        {
            [XmlAttribute("name")] public string name;

            [XmlArray("Text"), XmlArrayItem("Message")] public List<string> Message;
        }

        [XmlRoot("SignCollection")]
        public class SignDataContainer
        {
            [XmlArray("Signs")]
            [XmlArrayItem("Sign")]
            public List<SignData> signCollection = new List<SignData>();
        }
    }
}
