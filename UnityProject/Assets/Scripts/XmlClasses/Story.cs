﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlClasses
{
    public class Chapter
    {
        [XmlAttribute("name")]
        public string ChapterIdentifier = string.Empty;
        public string Text = string.Empty;
    }

    [XmlRoot("Story")]
    public class StoryReader
    {
        [XmlArray("Chapters")]
        [XmlArrayItem("Chapter")]
        public List<Chapter> ChapterCollection = new List<Chapter>();
    }
}