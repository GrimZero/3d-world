﻿using System.Diagnostics;

namespace PuzzleMechanics
{
    public static class PassableProperties
    {
        public enum Property
        {
            None,
            Electricity,
            Fire,
        }

        public static void Electricity()
        {
            Debug.Print("Electricity");
        }

        public static void Fire()
        {
            Debug.Print("Fire");
        }
    }
}
