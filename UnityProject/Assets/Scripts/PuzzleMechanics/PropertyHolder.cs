﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace PuzzleMechanics
{
    [RequireComponent(typeof(Collider))]
    public class PropertyHolder : MonoBehaviour
    {
        [HideInInspector] public UnityEvent activeProperty;
        [SerializeField] private PassableProperties.Property property;

        private void Start()
        {
            UpdateProperty(property);
        }

        public void UpdateProperty(PassableProperties.Property prop)
        {
            activeProperty = new UnityEvent();

            switch (prop)
            {
                case PassableProperties.Property.Electricity:
                    activeProperty.AddListener(PassableProperties.Electricity);
                    property = PassableProperties.Property.Electricity;
                    break;
                case PassableProperties.Property.Fire:
                    activeProperty.AddListener(PassableProperties.Fire);
                    property = PassableProperties.Property.Fire;
                    break;
            }
        }

        public void OnCollisionEnter(Collision collision)
        {
            var component = collision.transform.gameObject.GetComponent<PropertyHolder>();
            if (component && component.property != PassableProperties.Property.None)
            {
                UpdateProperty(component.property);
            }
        }

        public void ExecuteProperty()
        {
            activeProperty.Invoke();
        }
    }
}
