﻿using System.Collections.Generic;
using Inventory;
using UnityEngine;

namespace Editor.Extensions
{
    public static class ListUtilities
    {
        public static List<Texture> GetTextures(this List<Item> items)
        {
            List<Texture> sprites = new List<Texture>();
            foreach (var item in items)
            {
                sprites.Add(item.ItemSprite.texture);
            }

            return sprites;
        }
    }
}
