﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Extensions
{
    public static class SearchUtilities
    {
        public static List<T> GetAllInstances<T>() where T : ScriptableObject
        {
            List<T> a = new List<T>();
            foreach (var id in AssetDatabase.FindAssets("t:" + typeof(T).Name))
            {
                string path = AssetDatabase.GUIDToAssetPath(id);
                a.Add(AssetDatabase.LoadAssetAtPath<T>(path));
            }

            if (a.Count == 0)
            {
                a.Add(null);
            }

            return a;
        }
    }
}
