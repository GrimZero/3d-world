﻿using UnityEditor;
using UnityEngine;

namespace Editor.Extensions
{
    public static class TextureUtilities
    {
        public static T ReimportTexture<T>(string inPath, TextureImporterType type) where T : Object
        {
            TextureImporter importer = AssetImporter.GetAtPath(inPath) as TextureImporter;
            if (importer != null)
            {
                importer.textureType = type;
            }
            AssetDatabase.ImportAsset(inPath);
            AssetDatabase.Refresh();
            return AssetDatabase.LoadAssetAtPath<T>(inPath);
        }
    }
}
