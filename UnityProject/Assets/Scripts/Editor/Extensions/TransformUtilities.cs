﻿using System.Linq;
using UnityEditor;
using UnityEngine;

public static class TransformUtilities
{
    public static void RotateObject(Vector3 angle)
    {
        var objects = Selection.gameObjects.ToList();

        foreach (var gameObject in objects)
        {
            gameObject.transform.Rotate(angle, Space.Self);
        }
    }
}
