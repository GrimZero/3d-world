﻿using System;

namespace Editor.Extensions
{
    public static class StringUtilities
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp = StringComparison.OrdinalIgnoreCase)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }
}
