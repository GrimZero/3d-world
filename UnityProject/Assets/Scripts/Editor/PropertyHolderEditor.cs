﻿using PuzzleMechanics;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PropertyHolder))]
public class PropertyHolderEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Activate"))
        {
            (target as PropertyHolder)?.activeProperty.Invoke();
        }
    }
}
