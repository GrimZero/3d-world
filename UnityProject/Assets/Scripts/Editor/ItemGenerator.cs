﻿using Inventory;
using UnityEditor;
using Utilities;

namespace Editor.Tools
{
    public class ItemGenerator
    {
        private const string functionName = "Custom/Database/Add item";

        [MenuItem(functionName)]
        public static void AddToList()
        {
            Manager.Instance.ItemContainerObject.AddToList(Selection.gameObjects);
        }

        [MenuItem(functionName, true)]
        public static bool AddToListValidate()
        {
            if (Selection.gameObjects.Length == 0) return false;

            for (int i = 0; i < Selection.gameObjects.Length; i++)
            {
                var gO = Selection.gameObjects[i];
                if (!gO.GetComponent<Item>())
                {
                    return false;
                }
            }

            return true;
        }
    }
}
