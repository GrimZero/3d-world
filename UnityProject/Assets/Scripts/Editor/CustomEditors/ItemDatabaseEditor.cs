﻿using System;
using System.Collections.Generic;
using Editor.Extensions;
using Inventory;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Utilities;

namespace Editor.CustomEditors
{
    [CustomEditor(typeof(ItemDatabase))]
    public class ItemDatabaseEditor : UnityEditor.Editor
    {
        Vector2 scrollPos = Vector2.zero;
        string searchString = string.Empty;

        private int spacing = 10;

        private int selGridInt = 0;
        private List<Texture> elements = new List<Texture>();

        public void CreateMenu(Item item)
        {
            if (!item) return;

            EditorHelpers.CreateGenericMenu(
                new Tuple<bool, string, UnityAction, bool>(false, "Remove item from database",
                    () => { Manager.Instance.ItemContainerObject.RemoveFromList(item.gameObject); }, true),

                new Tuple<bool, string, UnityAction, bool>(false, "Generate item sprite",
                    () => { GenerateThumbnail.GenerateSprite(item.gameObject); }, false),

                new Tuple<bool, string, UnityAction, bool>(false, "Invoke item effect",
                    () => { item._useItem.Invoke(); }, false)
            );
        }

        public override void OnInspectorGUI()
        {
            searchString = EditorHelpers.SearchField(searchString);
            GUILayout.Space(5);

            elements.Clear();
            scrollPos = GUILayout.BeginScrollView(scrollPos);

            if (!Manager.Instance.ItemContainerObject) return;

            for (var i = 0; i < Manager.Instance.ItemContainerObject.Items.Count; i++)
            {
                //check if there are elements that are null and remove them
                if (!Manager.Instance.ItemContainerObject.Items[i])
                {
                    Manager.Instance.ItemContainerObject.Items.RemoveAt(i--);
                    continue;
                }

                //Show elements based on search string
                var item = Manager.Instance.ItemContainerObject.Items[i].GetComponent<Item>();
                if (item.ItemName.Contains(searchString, StringComparison.OrdinalIgnoreCase))
                {
                    elements.Add(item.ItemSprite.texture);
                }               
            }

            var xElements = Screen.width / (elements[0].width + spacing);
            if (elements.Count != 0 && EditorHelpers.CreateGridView(elements, spacing, xElements, GUIStyle.none, out selGridInt))
            {
                if (Event.current.button == (int)MouseButton.LeftMouse)
                {
                    if (Selection.activeGameObject == Manager.Instance.ItemContainerObject.Items[selGridInt].gameObject)
                    {
                        AssetDatabase.OpenAsset(Manager.Instance.ItemContainerObject.Items[selGridInt].gameObject);
                        SceneView.FrameLastActiveSceneView();
                    }

                    Selection.activeGameObject = Manager.Instance.ItemContainerObject.Items[selGridInt].gameObject;
                    EditorGUIUtility.PingObject(Selection.activeGameObject);
                    EditorUtility.FocusProjectWindow();
                }
                else if (Event.current.button == (int) MouseButton.RightMouse)
                {
                    CreateMenu(Manager.Instance.ItemContainerObject.Items[selGridInt].GetComponent<Item>());
                }
            }
            GUILayout.EndScrollView();
        }
    }
}
