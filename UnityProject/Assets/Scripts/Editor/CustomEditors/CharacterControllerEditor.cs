﻿using UnityEditor;

[CustomEditor(typeof(Utilities.CharacterController))]
public class CharacterControllerEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}
