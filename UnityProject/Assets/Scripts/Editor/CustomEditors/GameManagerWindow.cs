﻿using Editor;
using UnityEditor;
using UnityEngine;
using Utilities;

public class GameManagerWindow : EditorWindow
{
    private int selectedIndex = 0;

    private enum ToolbarOptions
    {
        Items,
        Options
    }

    [MenuItem("Custom/Windows/Manager")]
    static void Open()
    {
        GetWindow<GameManagerWindow>("Game manager");
    }

    void OnGUI()
    {
        GUILayout.Space(7);
        selectedIndex = EditorHelpers.ToolBar(selectedIndex, typeof(ToolbarOptions));

        EditorHelpers.BeginArea(new Rect(5, EditorGUIUtility.singleLineHeight * 2, Screen.width - 3, Screen.height));
        switch (selectedIndex)
        {
            case (int)ToolbarOptions.Items:
                EditorHelpers.OnEditorGUI(Manager.Instance.ItemContainerObject);
                break;
            case (int)ToolbarOptions.Options:
                break;
        }
        EditorHelpers.EndArea();
    }
}
