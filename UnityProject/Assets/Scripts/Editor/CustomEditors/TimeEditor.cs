﻿using UnityEditor;
using UnityEngine;
using Utilities;

namespace Editor.CustomEditors
{
    [CustomEditor(typeof(TimeCycle))]
    public class TimeEditor : UnityEditor.Editor
    {
        TimeCycle _timeCycle;

        public void OnEnable()
        {
            _timeCycle = (TimeCycle)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(_timeCycle.DateTime.ToString("HH:mm"), GUILayout.MaxWidth(50));
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.EnumPopup(_timeCycle.LocalTime);
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndHorizontal();

            EditorUtility.SetDirty(this);
        }
    }
}