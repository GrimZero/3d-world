﻿using Inventory;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Editor.CustomEditors
{
    [CustomEditor(typeof(Manager))]
    public class ManagerEditor : UnityEditor.Editor
    {
        private enum ToolbarOptions
        {
            Items,
            Player,
            Other,
            Files
        }

        private Manager _manager;

        public void OnEnable()
        {
            _manager = target as Manager;
        }

        public override void OnInspectorGUI()
        {
            _manager.LevelData = EditorHelpers<SceneData>.ObjectField(_manager.LevelData, false);
            GUILayout.Space(5);

            string[] tabNames = { "Items", "Player", "Other", "Files" };
            _manager.CurrTab = EditorHelpers.ToolBar(_manager.CurrTab, typeof(ToolbarOptions));

            GUILayout.Space(5);
            
            switch (_manager.CurrTab)
            {
                case (int)ToolbarOptions.Items:
                    _manager.ItemContainerObject = EditorHelpers<ItemDatabase>.ObjectField(_manager.ItemContainerObject, false);
                    _manager.Inventory = EditorHelpers<GameObject>.SmallLabelControl("Inventory",
                        () => EditorHelpers<GameObject>.ObjectField("Inventory", _manager.Inventory, true));
                    break;
                case (int)ToolbarOptions.Player: //Player
                    _manager.PlayerGameobject = EditorHelpers<GameObject>.SmallLabelControl(ToolbarOptions.Player.ToString(), 
                        () => EditorHelpers<GameObject>.ObjectField(ToolbarOptions.Player.ToString(), _manager.PlayerGameobject, true));
                    break;

                case (int)ToolbarOptions.Other: 
                    const string printerLabel = "Canvas";
                    _manager.Canvas = EditorHelpers<GameObject>.SmallLabelControl(printerLabel,
                        () => EditorHelpers<GameObject>.ObjectField(printerLabel, _manager.Canvas, true));
                    break;

                case (int)ToolbarOptions.Files:
                    EditorGUILayout.BeginHorizontal();
                    const string signLabel = "Sign";
                    _manager.SignXmlFile = EditorHelpers<TextAsset>.SmallLabelControl(signLabel,
                        () => EditorHelpers<TextAsset>.ObjectField(signLabel, _manager.SignXmlFile, false));

                    const string storyLabel = "Story";
                    _manager.StoryXmlFile = EditorHelpers<TextAsset>.SmallLabelControl(storyLabel,
                        () => EditorHelpers<TextAsset>.ObjectField(storyLabel, _manager.StoryXmlFile, false));
                    EditorGUILayout.EndHorizontal();
                    break;
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty((Manager)target);
            }
        }
    }
}
