﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;

namespace Editor.CustomEditors
{
    [CustomEditor(typeof(SceneData))]
    public class SceneIdsEditor : UnityEditor.Editor
    {
        private SceneData tar;

        void OnEnable()
        {
            tar = (SceneData) target;
            
            EditorBuildSettingsOnSceneListChanged();
            EditorBuildSettings.sceneListChanged += EditorBuildSettingsOnSceneListChanged;
        }

        public override void OnInspectorGUI()
        {
            for (int i = 0; i < tar.scenes.Keys.Count; i++)
            {
                var key = tar.scenes.Keys.ElementAt(i);

                GUILayout.BeginHorizontal();
                GUILayout.Label(SceneManager.GetSceneByName(key).buildIndex + ". " + key);
                tar.scenes[key] =
                    (SceneType) EditorGUILayout.EnumPopup(tar.scenes[key], GUILayout.Width(Screen.width / 2f));
                GUILayout.EndHorizontal();
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }
        }

        private void EditorBuildSettingsOnSceneListChanged()
        {
            List<string> sceneNames = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                var name = Path.GetFileNameWithoutExtension(scene.path);
                sceneNames.Add(name);

                if (!scene.enabled)
                {
                    tar.scenes.Remove(name);
                    continue;
                }

                if (!tar.scenes.ContainsKey(name))
                {
                    tar.scenes.Add(name, SceneType.Default);
                }
            }

            if (tar.scenes.Count != EditorBuildSettings.scenes.Length)
            {
                for (int i = 0; i < tar.scenes.Count; i++)
                {
                    if (!sceneNames.Contains(tar.scenes.Keys.ElementAt(i)))
                    {
                        tar.scenes.Remove(tar.scenes.Keys.ElementAt(i));
                    }
                }
            }
        }
    }
}