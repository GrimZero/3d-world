﻿using Inventory;
using UnityEditor;
using UnityEngine;

namespace Editor.CustomEditors
{
    [CustomEditor(typeof(Item))]
    public class ItemEditor : UnityEditor.Editor
    {
        private SerializedProperty CustomEvent;
        private SerializedProperty list;

        private Vector2 scrollDescription;

        public void OnEnable()
        {
            CustomEvent = serializedObject.FindProperty("_useItem");
            list = serializedObject.FindProperty("components");
        }

        public override void OnInspectorGUI()
        {
            Item item = (Item)target;

            int spriteSize = 52;
            int spacing = 10;
            int offsetLeft = 15;

            var width = Screen.width - spriteSize - (3 * spacing);
            EditorHelpers.BeginArea(new Rect(offsetLeft, 2, width, EditorGUIUtility.singleLineHeight * 4f));
            item.ItemName = EditorGUILayout.TextField(item.ItemName);
            item.StackSize = EditorGUILayout.IntField("Max stack size", item.StackSize);
            if (GUILayout.Button("Invoke Item effect (Runtime only)", new GUIStyle(GUI.skin.button){fontSize = 10}, GUILayout.Height(15)))
            {
                if (Application.isPlaying)
                {
                    item.Effect().Invoke();
                }
                else
                {
                    Debug.Log("Cannot invoke item effect while application is in edit mode.");
                }
            }
            EditorHelpers.EndArea();


            EditorHelpers.BeginArea(new Rect(offsetLeft + width - 8, 2, Screen.width - 8, EditorGUIUtility.singleLineHeight *4f));
            item.ItemSprite = (Sprite)EditorGUILayout.ObjectField(item.ItemSprite, typeof(Sprite), false, GUILayout.Height(spriteSize), GUILayout.Width(spriteSize));
            EditorHelpers.EndArea();

            GUILayout.Space(EditorGUIUtility.singleLineHeight * 3.5f + 3);
            scrollDescription = EditorGUILayout.BeginScrollView(scrollDescription, GUILayout.Height(EditorGUIUtility.singleLineHeight * 4f));
            item.ItemDescription = EditorGUILayout.TextArea(item.ItemDescription, GUILayout.ExpandHeight(true));
            EditorGUILayout.EndScrollView();

            EditorGUILayout.PropertyField(CustomEvent);
            EditorGUILayout.PropertyField(list);

            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(item);
            }
        }
    }
}
