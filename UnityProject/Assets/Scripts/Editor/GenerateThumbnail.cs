﻿using System.IO;
using System.Threading;
using Editor.Extensions;
using Inventory;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class GenerateThumbnail
    {
        private static string relativeDirectory = "Textures/Items";

        private const string itemName = "Custom/Database/Generate item sprite";

        [MenuItem(itemName)]
        public static void GenerateSprite()
        {
            GenerateSprite(Selection.activeGameObject);
        }

        [MenuItem(itemName, true)]
        public static bool GenerateSpriteValidate()
        {
            if (!Selection.activeGameObject) return false;

            return Selection.activeGameObject.GetComponent<Item>();
        }

        public static void GenerateSprite(GameObject gO)
        {
            var target = gO;

            Texture2D thumbTexture = null;
            while (!thumbTexture)
            {
                thumbTexture = AssetPreview.GetAssetPreview(target);
                Thread.Sleep(80);
            }

            var path = Path.Combine(Application.dataPath, relativeDirectory, target.name) + ".png";
            File.WriteAllBytes(path, thumbTexture.EncodeToPNG());

            var prefabRoot = PrefabUtility.LoadPrefabContents(AssetDatabase.GetAssetPath(target));
            try
            {
                var comp = prefabRoot.GetComponent<Item>();
                if (!comp)
                {
                    comp = prefabRoot.AddComponent<Item>();
                }

                comp.ItemSprite = TextureUtilities.ReimportTexture<Sprite>(Path.Combine("Assets", relativeDirectory, target.name + ".png"), TextureImporterType.Sprite);
                PrefabUtility.SaveAsPrefabAsset(prefabRoot, AssetDatabase.GetAssetPath(target));
            }
            finally
            {
                PrefabUtility.UnloadPrefabContents(prefabRoot);
            }
        }
    }
}
