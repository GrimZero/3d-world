﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace Editor
{
    public static class EditorHelpers<T> where T : class
    {
        public delegate T Delegate();

        public static T NamedControl(string name, Delegate @delegate)
        {
            GUI.SetNextControlName(name);
            return @delegate.Invoke();
        }

        public static T SmallLabelControl(string label, Delegate @delegate, int margin = 0)
        {
            EditorGUIUtility.labelWidth = EditorHelpers.CalcStringSize(label).x;
            return @delegate.Invoke();
        }

        public static T ObjectField(Object obj, bool allowSceneObject, bool disable = false)
        {
            EditorGUI.BeginDisabledGroup(disable);
            var returnedObject = EditorGUILayout.ObjectField(obj, typeof(T), allowSceneObject) as T;
            EditorGUI.EndDisabledGroup();

            return returnedObject;
        }

        public static T ObjectField(string label, Object obj, bool allowSceneObject, bool disable = false)
        {
            EditorGUI.BeginDisabledGroup(disable);
            var returnedObject = EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObject) as T;
            EditorGUI.EndDisabledGroup();

            return returnedObject;
        }
    }

    public static class EditorHelpers
    {
        public static EditorWindow GetFocusedWindow()
        {
            return EditorWindow.focusedWindow;
        }

        public static int GetGuiWidth()
        {
            return (int)EditorGUIUtility.currentViewWidth;
        }

        public static void BeginArea(Rect rect)
        {
            rect.xMax = rect.xMax - rect.xMin;
            rect.yMax = rect.yMax - rect.yMin;

            GUILayout.BeginArea(rect);
        }

        public static void EndArea()
        {
            GUILayout.EndArea();
        }

        public static Vector2 CalcStringSize(string str)
        {
            return GUI.skin.label.CalcSize(new GUIContent(str));
        }

        public static void BeginDisabledGroup()
        {
            EditorGUI.BeginDisabledGroup(true);
        }

        public static void FlexibleSpace()
        {
            GUILayout.FlexibleSpace();
        }

        /// <summary>
        /// Creates a search field with a clear button.
        /// </summary>
        /// <param name="value">The string variable storing the field input.</param>
        /// <returns>The changed value of the search field.</returns>
        public static string SearchField(string value)
        {
            GUILayout.BeginHorizontal();
            value = GUILayout.TextField(value, (GUIStyle) "SearchTextField");
            if (GUILayout.Button(string.Empty, (GUIStyle)"SearchCancelButton"))
            {
                value = string.Empty;
            }
            GUILayout.EndHorizontal();

            return value;
        }

        /// <summary>
        /// Creates a menu when one of the actions is performed.
        /// </summary>
        /// <param name="OnClick">
        /// <para>MouseButton indicates which button needs to be pressed to present the function.</para>
        /// <para>bool indicates play mode status (true = isPlaying)</para>
        /// <para>the string parameter is how the function is shown in menu (leaving empty will directly execute the function)</para>
        /// <para>UnityAction is the action to be executed</para>
        /// <para>this bool indicates whether or not to draw a separator</para></param>
        public static GenericMenu CreateGenericMenu(params Tuple<bool, string, UnityAction, bool>[] OnClick)
        {
            GenericMenu currentMenu = new GenericMenu();
            foreach (var keyValuePair in OnClick)
            {
                if (keyValuePair.Item1 == EditorApplication.isPlaying)
                {
                    if (keyValuePair.Item2 != string.Empty)
                    {
                        currentMenu.AddItem(new GUIContent(keyValuePair.Item2), false, keyValuePair.Item3.Invoke);
                    }
                    else
                    {
                        keyValuePair.Item3.Invoke();
                    }
                }

                if (keyValuePair.Item4)
                {
                    currentMenu.AddSeparator(string.Empty);
                }
            }

            if (currentMenu.GetItemCount() != 0)
            {
                currentMenu.ShowAsContext();
                Event.current.Use();
            }

            return currentMenu;
        }

        /// <summary>
        /// Creates a grid view that wraps horizontally
        /// </summary>
        /// <param name="elements"></param>
        /// <param name="spacing"></param>
        /// <param name="selectedIndex"></param>
        /// <returns>returns true if an element was clicked.</returns>
        public static bool CreateGridView(Texture[] elements, int spacing, int elementsPerLine, GUIStyle style, out int selectedIndex)
        {
            if (elements.Length > 0)
            {
                EditorGUI.BeginChangeCheck();
                selectedIndex = GUILayout.SelectionGrid(0, elements, elementsPerLine, style);
                return EditorGUI.EndChangeCheck();
            }

            selectedIndex = -1;
            return false;
        }

        public static bool CreateGridView(List<string> elements, int spacing, int elementsPerLine, GUIStyle style,out int selectedIndex)
        {
            if (elements.Count > 0)
            {
                EditorGUI.BeginChangeCheck();
                selectedIndex = GUILayout.SelectionGrid(0, elements.ToArray(), elementsPerLine, style);
                return EditorGUI.EndChangeCheck();
            }

            selectedIndex = -1;
            return false;
        }

        public static bool CreateGridView(List<Texture> elements, int spacing, int elementsPerLine, GUIStyle style, out int selectedIndex)
        {
            return CreateGridView(elements.ToArray(), spacing, elementsPerLine, style, out selectedIndex);
        }

        public static void OnEditorGUI(Object editor)
        {
            if (editor)
            {
                UnityEditor.Editor.CreateEditor(editor).OnInspectorGUI();
            }
        }

        public static int ToolBar(int selectedIndex, Type type)
        {
            try
            {
                return GUILayout.Toolbar(selectedIndex, Enum.GetNames(type));
            }
            catch (Exception)
            {
                Debug.Log(type + " is not of type enum");
            }

            return -1;
        }
    }
}