﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class ObjectExtensions
{
    public static List<T> GetAllInstances<T>() where T : ScriptableObject
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
        List<T> a = new List<T>();
        for (int i = 0; i < guids.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a.Add(AssetDatabase.LoadAssetAtPath<T>(path));
        }

        if (a.Count == 0)
        {
            a.Add(null);
        }

        return a;
    }
}
