﻿using UnityEditor;
using UnityEngine;
using Utilities.Extensions;

namespace Editor
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }

//Add properties from custom dictionaries for serialization -> [CustomPropertyDrawer(typeof(TYPENAME))]
    [CustomPropertyDrawer(typeof(StringObjectDictionary))]
    [CustomPropertyDrawer(typeof(StringSpriteDictionary))]
    [CustomPropertyDrawer(typeof(GameObjectFloatDictionary))]
    [CustomPropertyDrawer(typeof(ItemAmountDictionary))]
    public class DictionaryPropertyDrawerWrapper : SerializableDictionaryPropertyDrawer { }
}