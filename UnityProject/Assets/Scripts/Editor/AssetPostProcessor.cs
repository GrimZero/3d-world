﻿using System.IO;
using Inventory;
using UnityEditor;
using UnityEditor.Presets;
using UnityEngine;
using Utilities;

namespace Editor
{
    public class AssetPostProcessor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            for (var index = 0; index < importedAssets.Length; index++)
            {
                var importedAsset = importedAssets[index];

                var itemPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(importedAsset);
                if (itemPrefab && itemPrefab.GetComponent<Item>())
                {
                    Manager.Instance.ItemContainerObject.AddToList(itemPrefab);
                }
            }
        }

        void OnPreprocessAsset()
        {
            // Make sure we are applying presets the first time an asset is imported.
            if (assetImporter.importSettingsMissing)
            {
                // Get the current imported asset folder.
                var path = Path.GetDirectoryName(assetPath);
                while (!string.IsNullOrEmpty(path))
                {
                    // Find all Preset assets in this folder.
                    var presetGuids = AssetDatabase.FindAssets("t:Preset", new[] { path });
                    foreach (var presetGuid in presetGuids)
                    {
                        // Make sure we are not testing Presets in a subfolder.
                        string presetPath = AssetDatabase.GUIDToAssetPath(presetGuid);
                        if (Path.GetDirectoryName(presetPath) == path)
                        {
                            // Load the Preset and try to apply it to the importer.
                            var preset = AssetDatabase.LoadAssetAtPath<Preset>(presetPath);
                            if (preset.ApplyTo(assetImporter))
                                return;
                        }
                    }

                    // Try again in the parent folder.
                    path = Path.GetDirectoryName(path);
                }
            }
        }
    }
}
