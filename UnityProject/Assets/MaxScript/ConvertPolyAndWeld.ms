for data in selection do
(
	for Obj in $Geometry do
	(
		convertToMesh Obj
		meshop.weldVertsByThreshold Obj Obj.verts 0.1
		convertToPoly Obj
	)
)
	