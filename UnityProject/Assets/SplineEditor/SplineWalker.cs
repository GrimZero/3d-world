﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Utilities.Extensions;

namespace SplineEditor
{
    public class SplineWalker : MonoBehaviour
    {
        [Tooltip("The spline for this object to follow"), HideInInspector] public BezierSpline spline;
        [Tooltip("The time in seconds required to go through the spline")]public float duration;
        [Tooltip("Whether or not to follow the orientation of the spline")]public bool lookForward;
        [Tooltip("The mode to be used by the walker")] public SplineWalkerMode mode;
        [Tooltip("The interval to use for the buttons")] public float interval = 0.01f;

        [HideInInspector] public float progress;
        private bool goingForward = true;

        [Tooltip("Define the objects that need to chase this and set their offset (offsets are relative to previous element)")]
        public GameObjectFloatDictionary childrenList = new GameObjectFloatDictionary();

        public static void Allign(GameObject first)
        {
            var walker = first.GetComponent<SplineWalker>();
            first.transform.position = walker.spline.GetPoint(walker.progress);

            float cummul = 0f;

            for (int i = 0; i < walker.childrenList.Count; i++)
            {
                cummul += walker.childrenList[walker.childrenList.Keys.ElementAt(i)];
                walker.childrenList.Keys.ElementAt(i).transform.position = walker.spline.GetPoint(walker.progress - cummul);
            }

            if (walker.lookForward)
            {
                first.transform.LookAt(first.transform.position + walker.spline.GetDirection(walker.progress));

                cummul = 0;
                for (int i = 0; i < walker.childrenList.Count; i++)
                {
                    cummul += walker.childrenList[walker.childrenList.Keys.ElementAt(i)];
                    walker.childrenList.Keys.ElementAt(i).transform.LookAt(walker.childrenList.Keys.ElementAt(i).transform.position + walker.spline.GetDirection(walker.progress - cummul));
                }
            }
        }

        private void Update ()
        {
            if (goingForward)
            {
                progress += Time.deltaTime / duration;
                if (progress > 1f)
                {
                    if (mode == SplineWalkerMode.Once)
                    {
                        progress = 1f;
                    }
                    else if (mode == SplineWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else
                    {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }
            }
            else
            {
                progress -= Time.deltaTime / duration;
                if (progress < 0f)
                {
                    progress = -progress;
                    goingForward = true;
                }
            }

            Allign(gameObject);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(SplineWalker))]
    public class SplineWalkerEditor : UnityEditor.Editor
    {
        private float min = 0, max = 1;

        public override void OnInspectorGUI()
        {
            var o = (SplineWalker)target;
            o.spline = EditorGUILayout.ObjectField("Spline", o.spline, typeof(BezierSpline), true) as BezierSpline;

            DrawDefaultInspector();

            if (o.interval > max || o.interval < min)
            {
                o.interval = 0.05f;
            }

            GUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            if (GUILayout.Button("-", GUILayout.Width(20)))
            {
                o.progress -= o.interval;
            }

            o.progress = GUILayout.HorizontalSlider(o.progress, 0, 1);
            if (GUILayout.Button("+", GUILayout.Width(20)))
            {
                o.progress += o.interval;
            }

            GUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck())
            {
                if (o.progress < 0)
                {
                    o.progress = 0;
                }
                else if (o.progress > 1)
                {
                    o.progress = 1;
                }
            }

            if (o.spline)
            {
                SplineWalker.Allign(Selection.activeGameObject);
            }
        }
    }
#endif
}