﻿namespace SplineEditor
{
    public enum BezierControlPointMode {
        Free,
        Aligned,
        Mirrored
    }
}