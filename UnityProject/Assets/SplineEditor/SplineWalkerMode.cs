﻿namespace SplineEditor
{
    public enum SplineWalkerMode {
        Once,
        Loop,
        PingPong
    }
}